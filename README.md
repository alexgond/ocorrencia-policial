# Aplicação de Classificação de Ocorrências Policiais

## Nome
# Alex Gondim Lima

## Descrição
# Esta aplicação utiliza um modelo de machine learning para classificar ocorrências policiais em diferentes categorias com base em descrições textuais. 
# A aplicação é construída utilizando FastAPI para servir uma API e Firebase Firestore para armazenar as previsões. 
# A interface web permite que os usuários cadastrem novas ocorrências e vejam as previsões de categoria.

## Estrutura do Projeto
# Atividade1_MLDevops/
# ├── app/
# │   ├── __init__.py
# │   ├── main.py
# │   ├── modelo.pkl
# │   └── firebase_key.json
# ├── static/
# │   ├── index.html
# │   
# │     
# ├── tests/
# │   └── test_main.py    # Arquivo de testes unitários
# ├── requirements.txt
# ├── README.md
# ├── Dockerfile
# ├── .gitlab-ci.yml
# └── .env

## Dependências
# - fastapi==0.95.2
# - uvicorn==0.22.0
# - scikit-learn==1.2.2
# - firebase-admin==5.2.0
# - pandas==2.1.0
# - numpy==1.25.0
# - starlette==0.27.0


## Configuração do Ambiente
# 1. Crie e ative um ambiente virtual:
#    ```bash
#    python -m venv venv
#    source venv/bin/activate  # No Windows use `venv\Scripts\activate`
#    ```

# 2. Instale as dependências listadas no `requirements.txt`:
#    ```bash
#    pip install -r requirements.txt
#    ```

# 3. Baixe os pacotes NLTK necessários:
#    ```python
#    import nltk
#    nltk.download('stopwords')
#    nltk.download('wordnet')
#    ```

# 4. Adicione seu arquivo de chave Firebase (JSON) na pasta `app/` e renomeie para `firebase_key.json`.

## Execução da API
# 1. Execute o servidor FastAPI:
#    ```bash
#    uvicorn app.main:app --reload
#    ```

# 2. Acesse a página HTML no navegador:
#    ```
#    http://localhost:8000/static/index.html
#    ```

## Teste da API
# - Use a página HTML para cadastrar novas ocorrências e visualizar as previsões de categoria.
# - Ao preencher a descrição da ocorrência e sair do campo de texto, a categoria será preenchida automaticamente.
# - Clique em "Enviar" para cadastrar a ocorrência e limpar os campos do formulário.

## Execução com Docker
### Construir a Imagem Docker
# No diretório raiz do projeto, execute o seguinte comando para construir a imagem Docker:
#    ```bash
#    docker build -t classificacao-ocorrencias .
#    ```

### Executar o Contêiner Docker
# Para rodar a aplicação em um contêiner Docker, execute o seguinte comando:
#    ```bash
#    docker run -p 8000:8000 classificacao-ocorrencias
#    ```

# A aplicação estará acessível em `http://localhost:8000`.

## Conclusão
# Este projeto demonstra a integração de machine learning, FastAPI e Firebase Firestore para criar uma aplicação de classificação de ocorrências policiais. 
# A interface web permite a interação fácil com a API e a visualização das previsões.
