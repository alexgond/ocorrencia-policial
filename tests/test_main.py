import unittest
from app.main import classificar_ocorrencia

class TestMain(unittest.TestCase):
    def test_classificar_ocorrencia(self):
        descricao = "Roubo seguido de morte"
        categoria_esperada = "Crimes de trânsito"  # Ajuste conforme a saída real do modelo
        categoria_predita = classificar_ocorrencia(descricao)
        self.assertEqual(categoria_predita, categoria_esperada)

if __name__ == '__main__':
    unittest.main()
