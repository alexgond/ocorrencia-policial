from fastapi import FastAPI, HTTPException
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
import pickle
import pandas as pd
import firebase_admin
from firebase_admin import credentials, firestore
from fastapi.middleware.cors import CORSMiddleware
import joblib
from sklearn.feature_extraction.text import CountVectorizer

app = FastAPI()

# Carregar o modelo treinado
with open("app/modelo.pkl", "rb") as f:
    model_data = pickle.load(f)
    modelo = model_data["modelo"]
    vectorizer = model_data["vectorizer"]

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/static", StaticFiles(directory="static"), name="static")

credenciais = credentials.Certificate("app/firebase_key.json")
firebase_admin.initialize_app(credenciais)
db = firestore.client()

class DadosEntrada(BaseModel):
    descricao: str

@app.post("/prever")
def prever(dados: DadosEntrada):
    df = pd.DataFrame([dados.dict().values()], columns=dados.dict().keys())
    X_tfidf = vectorizer.transform(df["descricao"])
    try:
        previsao = modelo.predict(X_tfidf)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return {"categoria": previsao[0]}

@app.post("/salvar_previsao")
def salvar_previsao(dados: DadosEntrada):
    df = pd.DataFrame([dados.dict().values()], columns=dados.dict().keys())
    X_tfidf = vectorizer.transform(df["descricao"])
    try:
        previsao = modelo.predict(X_tfidf)[0]
        doc_ref = db.collection('previsoes').document()
        doc_ref.set({
            'descricao': dados.descricao,
            'categoria': previsao
        })
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    return {"categoria": previsao}

def classificar_ocorrencia(descricao):
    X_tfidf = vectorizer.transform([descricao])
    predicao = modelo.predict(X_tfidf)
    return predicao[0]
